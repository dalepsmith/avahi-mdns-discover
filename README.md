
This is the companion native application for the mdns-discover Firefox
add-on that uses avahi for mDNS.

On a Debian system, you need to have these package installed:
`build-essential libavahi-core-dev libavahi-client-dev libjson-c-dev`
and either `pkgconf` or `pkg-config`.

A `make install-local` will place the manifest file and the executable in
your personal Firefox area.

The `make install` target will make it available for all users.
Note that you must have write permissions in the
`/usr/lib/mozilla/native-messaging-hosts/` to do this.
