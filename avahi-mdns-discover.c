#define PROTO AVAHI_PROTO_INET

#define DEBUG_OUT 0

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>


#include <avahi-common/thread-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>

#include <json-c/json.h>

AvahiThreadedPoll *poller = 0;
AvahiClient *client = 0;

AvahiServiceBrowser *browser;

void
send_output(const char* text)
{
#if DEBUG_OUT
  printf("%d: '%s'\n", strlen(text), text);
  fflush(stdout);
#else
  int len = strlen(text);
  write(STDOUT_FILENO, &len, 4);
  write(STDOUT_FILENO, text, len);
#endif
}

static void
resolve_callback(
    AvahiServiceResolver *r,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    void* userdata)
{
  assert(r);

  /* Called whenever a service has been resolved successfully or timed out */

  switch (event) {
  case AVAHI_RESOLVER_FAILURE:
    break;

  case AVAHI_RESOLVER_FOUND:
    {
      struct json_object *jo, *tx;
      AvahiStringList *l;
      char a[AVAHI_ADDRESS_STR_MAX];

      avahi_address_snprint(a, sizeof a, address);
      jo = json_object_new_object();
      json_object_object_add(jo, "type",     json_object_new_string("add"));    
      json_object_object_add(jo, "srv",      json_object_new_string(type));
      json_object_object_add(jo, "service",  json_object_new_string(name));
      json_object_object_add(jo, "hostname", json_object_new_string(host_name));
      json_object_object_add(jo, "address",  json_object_new_string(a));
      json_object_object_add(jo, "port",     json_object_new_int(port));
      tx = json_object_new_object();
      for (l=txt; l; l=avahi_string_list_get_next(l)) {
        char *key=0, *val=0;
        size_t len=0;
        if (!avahi_string_list_get_pair(l, &key, &val, &len)) {
          if (len) {
            json_object_object_add(tx, key, json_object_new_string_len(val, len));
            avahi_free(val);
          } else {
            json_object_object_add(tx, key, (struct json_object *) 0);
          }
          avahi_free(key);
        }
      }
      json_object_object_add(jo, "txt", tx);
      char const *s = json_object_to_json_string_ext(jo, JSON_C_TO_STRING_PLAIN);
      send_output(s);
      json_object_put(jo);
    }
  }

  avahi_service_resolver_free(r);
}

static void
browse_callback(
                AvahiServiceBrowser *b,
                AvahiIfIndex interface,
                AvahiProtocol protocol,
                AvahiBrowserEvent event,
                const char *name,
                const char *type,
                const char *domain,
                AvahiLookupResultFlags flags,
                void* userdata)
{
  assert(b);
  
  switch (event) {
  default:
    break;

  case AVAHI_BROWSER_FAILURE:
    fprintf(stderr, "%s: %s\n", __func__, avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
    avahi_threaded_poll_quit(poller);
    return;

  case AVAHI_BROWSER_NEW:
    if (!(avahi_service_resolver_new(avahi_service_browser_get_client(b), interface, protocol, name, type, domain, PROTO, 0, resolve_callback, userdata)))
      fprintf(stderr, "%s: Failed to resolve service '%s': %s\n",
              __func__, name, avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
    
    break;

  case AVAHI_BROWSER_REMOVE:
    {
      // build json object
      struct json_object *jo = json_object_new_object();
      json_object_object_add(jo, "type", json_object_new_string("remove"));
      json_object_object_add(jo, "srv", json_object_new_string(type));
      json_object_object_add(jo, "service", json_object_new_string(name));
      char const *s = json_object_to_json_string_ext(jo, JSON_C_TO_STRING_PLAIN);
      send_output(s);
      json_object_put(jo);
    }
    break;
    
  case AVAHI_BROWSER_ALL_FOR_NOW:
  case AVAHI_BROWSER_CACHE_EXHAUSTED:
    break;
  }
}

static void
client_callback(AvahiClient *c, AvahiClientState state, void * userdata)
{
  assert(c);

  /* Called whenever the client or server state changes */
  switch (state) {
  default:
  case AVAHI_CLIENT_S_REGISTERING:
    break;
  case AVAHI_CLIENT_S_RUNNING:
    break;
  case AVAHI_CLIENT_S_COLLISION:
    break;
  case AVAHI_CLIENT_CONNECTING:
    break;
  case AVAHI_CLIENT_FAILURE:
    fprintf(stderr, "%s: Server connection failure: %s\n", __func__, avahi_strerror(avahi_client_errno(c)));
    avahi_threaded_poll_quit(poller);
  }
}

static void
handler(int sig)
{
  fprintf(stderr, "%s:\n", __func__);
  // avahi_threaded_poll_stop(poller);
  //printf("%s: after\n", __func__);
}

int
init_client()
{
  int error;

  /* Allocate main loop object */
  if (!(poller = avahi_threaded_poll_new())) {
    fprintf(stderr, "%s: Failed to create thread poll object.\n", __func__);
    return 1;
  }

  /* Allocate a new client */
  client = avahi_client_new(avahi_threaded_poll_get(poller), 0, client_callback, NULL, &error);
  /* Check whether creating the client object succeeded */
  if (!client) {
    fprintf(stderr, "%s: Failed to create client: %s\n", __func__, avahi_strerror(error));
    return 1;
  }

  return 0;
}

AvahiServiceBrowser *
start_browser(const char *type)
{
  AvahiServiceBrowser *sb = 0;
  /* Create the service browser */
  if (!(sb = avahi_service_browser_new(client, AVAHI_IF_UNSPEC, PROTO, type, NULL, 0, browse_callback, NULL))) {
    fprintf(stderr, "%s: Failed to create service browser: %s\n", __func__,
            avahi_strerror(avahi_client_errno(client)));
  }
  return sb;
}

// Read 4 bytes,
// Alloc memory for length
// Read length into memory
// Return true
// Return false on EOF
int
read_object(char **out)
{
  int len;
  int ret = read(STDIN_FILENO, &len, 4);
  if (ret != 4)
    return 0;

  char *buffer = malloc(len+1);
  if (!buffer)
    return 0;

  buffer[len] = 0;
  char *bp = buffer;
  while (len) {
    int ret = read(STDIN_FILENO, bp, len);
    if (ret <= 0) {
      free(buffer);
      return 0;
    }
    bp += ret;
    len -= ret;
  }
  *out = buffer;
  return 1;
}

int
main(int argc, char *argv[])
{
  // argv[1]: The complete path to the app manifest.
  // argv[2]: the ID of the add-on that started this.
  int ret = 1;
  
  struct sigaction sa;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sa.sa_handler = handler;
  if (sigaction(SIGINT, &sa, NULL) == -1)
    goto fail;

  if (init_client())
    goto fail;

  // See http://avahi.org/wiki/RunningAvahiClientAsThread
  // avahi_threaded_poll_lock(poller);
  if (avahi_threaded_poll_start(poller)) {
    fprintf(stderr, "%s: Failed to start thread poll object.\n", __func__);
    goto fail;
  }
  // avahi_threaded_poll_unlock(poller);

  /* Run the main loop */
  //printf("%s: before loop\n", __func__);
  //select(0, 0, 0, 0, 0);
  //printf("%s: after loop\n", __func__);
  
  char *buffer = 0;
  while (read_object(&buffer)) {
#if 1
    // Parse the JSON
    struct json_object *parsed_json = json_tokener_parse(buffer);
    avahi_threaded_poll_lock(poller);
    if (browser)
      avahi_service_browser_free(browser);
    browser = start_browser(json_object_get_string(parsed_json));
    avahi_threaded_poll_unlock(poller);
    json_object_put(parsed_json);
#else
    // Just lop off the double quotes
    if (buffer[0] == '"' && strchr(&buffer[1], '"')) {
      strchr(&buffer[1], '"') = 0;
      avahi_threaded_poll_lock(poller);
      if (browser)
        avahi_service_browser_free(browser);
      browser = start_browser(&buffer[1]);
      avahi_threaded_poll_unlock(poller);
    }
#endif
    free(buffer);
    if (!browser)
      goto fail;
  }

  ret = 0;
  
 fail:
  if (poller)
    avahi_threaded_poll_stop(poller);

  if (browser)
    avahi_service_browser_free(browser);
  
  if (client)
    avahi_client_free(client);

  if (poller)
    avahi_threaded_poll_free(poller);

  return ret;
}
