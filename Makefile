
all: avahi-mdns-discover

# apt install libavahi-core-dev libavahi-client-dev
AVAHI := $$(pkg-config avahi-client --cflags --libs) 

# apt install libjson-c-dev
JSON := $$(pkg-config json-c --libs)

# also need
# apt install {pkgconf|pkg-config} build-essential


%: %.c
	$(CC) -o $@ -Wall $^ -D_POSIX_C_SOURCE $(AVAHI) $(JSON) -pthread

clean:
	$(RM) -f avahi-mdns-discover

USRDIR := $$HOME/.mozilla/native-messaging-hosts
install-local: all mdns.discover.firefox.json.in
	mkdir -p $(USRDIR)
	sed -e "s|@DIR@|$(USRDIR)|" mdns.discover.firefox.json.in > $(USRDIR)/mdns.discover.json
	cp -f avahi-mdns-discover $(USRDIR)/

uninstall-local:
	$(RM) -f $(USRDIR)/mdns.discover.json
	$(RM) -f $(USRDIR)/avahi-mdns-discover

LIBDIR = /usr/lib/mozilla/native-messaging-hosts
install: all mdns.discover.firefox.json.in
	mkdir -p $(LIBDIR)
	sed -e "s|@DIR@|$(LIBDIR)|" mdns.discover.firefox.json.in > $(LIBDIR)/mdns.discover.json
	cp -f avahi-mdns-discover $(LIBDIR)/

uninstall:
	$(RM) -f $(LIBDIR)/avahi-mdns-discover
	$(RM) -f $(LIBDIR)/mdns.discover.json
